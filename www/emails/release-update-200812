To: debian-devel-announce@lists.debian.org
Subject: Release Update: d-i RC2 and deep freeze; handling of remaining RC bugs; *-reports and release notes

Hello,

We're sorry there hasn't been a release update from us in several
months. We had a clear picture of the missing bits, and tracked their
progress; in the meantime, we entertained ourselves with the flood of
unblock requests.


The missing bits
================

Currently, the only extra piece we need to declare the Lenny puzzle
ready is a final version of the installer. The -boot people are about to
deliver a second release candidate, which will be final unless something
critical pops up.


Deep freeze
===========

When d-i RC2 appears, we'll enter deep freeze. This has important
consequences for package maintainers:

    * unblock requests sent after d-i RC2 will only be considered if
      they fix RC bugs, and nothing else.

We'll deal with requests that were sent previously on a best-effort,
from oldest to newest basis. If you need to send a ping about a request,
reply to your original mail, don't start a new thread.


The RC bug count
================

At the moment of deep freeze, there will still be a good number of RC
bugs affecting Lenny. The release team will go over that list, and try
to apply a sensible solution that allows us to target a release, if at
all possible, two weeks after declaring d-i final.

This means we'll have a "short list" of bugs that must absolutely be
fixed for Lenny, and we'll ignore, downgrade, or do removals for the
rest as most appropriate in each case.

However, this does not mean the effort to fix any RC bug should stop. We
should strive for the best possible Lenny, and the release team will
review all uploads fixing RC bugs, and ensure they migrate on time.

Also, bugs that don't make it into the short list and don't get a fix in
time for Lenny can still be fixed in a point release, as usual, if an
appropriate fix exists.


Upgrade/Installation reports, and the Release Notes
===================================================

Help is needed in this area. On one hand:

    * since Lenny is almost in its final form, it is a good time for the
      brave to upgrade their systems to Lenny, and inform of any troubles
      by filing a bug against the upgrade-reports package.

    * if you have new systems to install, testing of the Debian
      Installer RC2 (or the daily builds) would be most welcome. As
      usual, please report any problems by filing bugs against the
      installation-reports package.

    Full security support is provided for Lenny already by the Security
    Team, though for the kernel updates will be done through unstable
    until Lenny releases.

On the other hand, we also need help in processing those bug reports,
particularly those filed against upgrade-reports. If you think you could
help with this, please do! Work involves figuring out what went wrong
with the upgrade, filing bug against the involved packages if
appropriate, and/or documenting the issue or workarounds in the Release
Notes.


The firmware GR
===============

With these hopefully solid plans in place for the release, we feel the
need to acknowledge that there is an ongoing vote whose outcome could
potentially disrupt them. In the release team we sincerely believe that
we've not drifted apart from the project as a whole with our take on
this matter to date. If this GR were to prove us wrong, we'd suck it up,
stand corrected, and come with a plan that doesn't involve ignoring the
outcome of the GR.

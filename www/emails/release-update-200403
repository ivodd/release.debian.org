THIS IS A DRAFT
THIS IS A DRAFT
THIS IS A DRAFT

From: Colin Watson <cjwatson@debian.org>
To: debian-devel-announce@lists.debian.org
Subject: Release update

It's been a little longer than it should have been since the last one.
Sorry. This is a not-terribly-formal update to get some news out despite
the members of the release team being somewhat snowed under with other
things at the moment.

So, once again, where are we?

debian-installer beta3 was released on 15 March as planned, and seems to
be holding up fairly well. It was still short of some architectures,
though: namely arm, hppa, mipsel, powerpc, and s390. Development on all
of these is continuing, and we should have a beta3 update soon to add a
couple of those missing architectures and fix up some other errata. The
plan from now on is to release d-i betas at roughly monthly intervals,
so expecting beta4 in the second half of April, with beta5 being the one
we aim to release as stable after a further month or so of testing.

If you have some spare time and want to help Debian release, working on
debian-installer should be your number one priority. Without an
installer, we don't release; architectures without a working d-i won't
be candidates for releasing. For myself, I found it surprisingly easy to
get started with the daily netinst images and a batch of CD-Rs until I
figured out how to build my own netboot images. Go for it.

We want to freeze for as short a time as possible. Long freezes are a
serious impediment (those who remember the potato freeze will know what
I mean), and cause no little confusion, so we'll only freeze completely
once the d-i schedule for a stable release is absolutely clear. However,
we do need to stop base system churn causing problems for d-i. To that
end:

  * As of now, no new packages will be added to the base system. This
    means that packages in the base system *must not* change their
    package relationships.

  * Large changes to the base system must be cleared with the release
    team and the d-i team before being uploaded to unstable.

  * Changes to the base system should avoid regressing translations.

  * Where possible, avoid new major upstream versions of other packages.
    If in any doubt about whether an upgrade is appropriate, contact the
    release team.

Elsewhere, please keep on fixing RC bugs, particularly in packages that
are important enough that removal isn't an option.
http://bugs.qa.debian.org/ shows some of these.

Less important packages with unfixed RC bugs are very likely to be
removed.


Let's have a quick timeline. Since d-i is the main blocker, we'll lay it
out to correspond with its milestones.

By d-i beta4 (fourth week of April):

  * The installer should work fairly reliably on all release candidate
    architectures (which allows plenty of time to test all of them).
    We've stopped base system dependency churn, so it should be able to
    stay working, unlike earlier betas.

  * If there are architectures still not working at all by this point,
    we'll have to drop some. Anything that didn't release with beta3
    should be getting concerned about this (particularly hppa, mipsel,
    and s390), so, if you own such a box, please try out d-i and fix any
    bugs you find.

  * All changes to the base system should be complete, barring perhaps a
    few tweaks critical for d-i.

  * All major changes to the rest of the distribution should be
    complete.

  * Major changes to the rest of the distribution should not happen
    after beta4 is released, particularly including significant
    dependency rearrangements that might affect testing.

d-i beta5 (fourth week of May):

  * The installer should work well on all release candidate
    architectures, and should be almost fully debugged.

  * Once we've had enough installation reports to know that this beta is
    OK, we'll start the formal freeze. Any absolutely necessary changes
    are likely to take place via testing-proposed-updates.

  * The security team will have a couple of weeks after beta5 to catch
    up with security advisories.

If necessary, d-i final:

  * Minor and necessary changes from d-i beta5.

  * Release sarge after a couple of days of final testing.


There we have it. Now let's make it work. :-)

Cheers,

-- 
Colin Watson                                       [cjwatson@debian.org]
Debian Release Assistant, on behalf of the release team

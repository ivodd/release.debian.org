THIS IS A DRAFT
THIS IS A DRAFT
THIS IS A DRAFT

From: Steve Langasek <vorlon@debian.org>
To: debian-devel-announce@lists.debian.org
Subject: Release update: Qt, arts, arm, yes; freeze date, no

Hello, world.

After three weeks since the last update, the status of the release is as
follows.

The package backlog for arm has all but cleared up; the queue of
packages waiting to be built on arm is still longer than for other
archs, but it no longer appears to be a blocking issue for sarge.

Draining the queue on arm uncovered an RC bug in qt-x11-free 3.3.3 at
the beginning of September, which failed to build from source only on
arm.  This bug has been fixed, and the new upstream version of
qt-x11-free, currently the top blocker keeping other package updates out
of sarge[1], reaches testing today.  This also partly accounts for the
latest jump in the size of the arm build queue, as packages needing the
missing libqt-mt-dev have recently become eligible again for building.

With it, arts 1.3 (the second biggest blocker keeping other package
updates out of sarge) is also propagating to testing.

The #3 blocker, kdelibs 3.3, is still not being considered as a
candidate for sarge.  Release-critical updates to KDE packages still
need to be uploaded to testing-proposed-updates.

The last release update mentioned that in the toolchain, gcc was fixed
in sarge but binutils fixes were pending.  The binutils problems on hppa
have been resolved, and a partial fix has been implemented for the
binutils problems on mips.  This latter fix has revealed the need for
one more round of updates to gcc-3.3.  Matthias Klose, the gcc-3.3
maintainer, has indicated he'll be uploading that fix this weekend.
Once that's done, we'll be revisiting the question of getting the proper
set of toolchain fixes into sarge.


The bad news is that we still do not have an ETA for the
testing-security autobuilders to be functional.  This continues to be
the major blocker for proceeding with the freeze; we would /like/ to
have security support in place for sarge before encouraging widespread
upgrade woody->sarge upgrade testing, but we /need/ to have it in place
before releasing, so it would be unwise to try to freeze the rest of the
archive without any confirmed schedule for the last stages of the
release.

We are also still missing official autobuilders for
testing-proposed-updates on alpha and mips.  All other architectures
appear to be keeping up with t-p-u uploads.  This is causing some
moderate delays getting fixes into testing via t-p-u.  If you have
specific concerns about packages that you've uploaded to t-p-u, please
contact debian-release@lists.debian.org.


While the toolchain issues and autobuilder queues are being sorted out,
there are still a few outstanding items to keep the rest of us busy.

The installer team is moving towards a debian-installer test candidate,
which will include 2.4.27/2.6.8 kernels and various other fixes against
RC1[2].  Testing on all architectures is always needed to help prepare
such d-i releases, in addition to testing once the builds are released.
The installer team can always use help in processing install reports as
well.  Please contact debian-boot@lists.debian.org if you're willing to
help out.

There has been discussion about providing transitional kernel packages
for woody->sarge upgrades, so that true-i386 machines can continue to be
supported using instruction emulation.  Unfortunately, the instruction
emulation patch is not as mature as previously believed.  The Debian
kernel team is working on an i486 emulation patch that can be included
in the kernels for sarge.  We have also learned that woody->sarge
upgrades are currently not possible on sparc32 and hppa64 systems, due
to glibc/kernel incompatibilities.  The hppa64 case still requires some
investigation, but the sparc32 case definitely requires a kernel that
provides the instructions needed by the current glibc.  Josh Kwan has
volunteered to provide the necessary packages for sparc upgrades.
Anyone interested in helping with any of these upgrade issues should
coordinate with debian-kernel@lists.debian.org.

Last but not least, the count of release critical bugs affecting
sarge[3] has been dropping slower than originally projected.  The
original timeline would have us freezing with about 100 RC bugs left;
with 160-170 bugs left today, this still puts us a long way from the
freeze.  There's plenty of room for patch submissions and NMUs here.
Removals for packages outside of base+standard that have open RC bugs
will be happening at an accelerated pace, so if you see RC bugs listed
on packages you use (or maintain), now's the time to do something about
it.


Package uploads for sarge should continue to be made according to the
following guidelines.

  - If your package is frozen, but there are no changes in unstable that
    should not also be in testing, upload to unstable only and contact
    debian-release@lists.debian.org if changes need to be pushed through
    to testing.
  - If your package is frozen and the version in unstable includes
    changes that should NOT be part of sarge, contact
    debian-release@lists.debian.org with details about the changes you
    plan to upload (diff -u preferred) and, with approval, upload to
    testing-proposed-updates.  Changes should be limited to translation
    updates and fixes for important or RC bugs.
  - If your package depends on KDE and you have changes you need to make
    to fix important or RC bugs for sarge, you will need to upload to
    testing-proposed-updates as well, with the same requirements as for
    frozen packages.
  - All other package updates should be uploaded to unstable only.
    Please use urgency=high for uploads that fix RC bugs for sarge
    unless you believe there is a significant chance of breakage in the
    new package.


Finally, let's recap the original timeline, with the fixed dates traded
out for variables, to see where we're going once testing-security is
on-line...

  N+0 days
  ~140 RC bugs
  testing-proposed-updates, testing-security working for all
  architectures
  Official security support for sarge begins

The testing-proposed-updates queue is already in use for uploads of RC
bugfixes, but up to now testing-security is not in use.  Now that it's
ready, the security team can begin providing security support for sarge.
The sooner this can actually happen, the better.

With security support in place, adventurous users can begin testing the
upgrade path from woody to sarge.  Their feedback will be used for
further bugfixing of packages, and to start preparing release notes.


  N+5 days
  120 RC bugs
  Last call for low-urgency uploads

Although the original "last call" has come and gone, the doors are not
closed for further low-priority fixes to make there way into sarge until
after testing-security is up and running.

This allows us a period of time after the start of upgrade testing for
low-priority fixes to get uploaded if they're targetted for sarge.  Of
course, bugs that warrant uploads at a higher urgency can be fixed after
this date; including t-p-u uploads for RC bugs.


  N+16 days
  90 RC bugs
  d-i RC2
  Freeze time!

A release candidate of d-i will require about three weeks to prepare, so
preparations will need to begin before testing-security is fully
operational to meet this timeline.  Because this process has not started
yet, and because of the Oldenburg conference that will keep many of our
installer team busy, this gives us a date for "N" of at least two weeks
out from today.

If all goes well, for both RC bug fixes and the d-i RC2 release, we'll
be ready to freeze the rest of the archive at this point.  Changes to
base+standard are limited to RC fixes only; fixes for RC bugs and
translation updates are allowed for the rest of the archive via
testing-proposed-updates.


  N+30 days
  0 RC bugs

Any remaining release-critical bugs will be fixed through uploads to
testing-proposed-updates or by removals from sarge.

With a final cut of the installer in the bag, we will also be fixing any
remaining CD generation problems, as well as final tweaks to the
installation manual and release notes.  Before this point, we will also
need to have kernel upgrade paths sorted out for all architectures.

Around this time, we will be able to set a date for the full release.


Keep up the good work, folks.  We're definitely on the road to release,
even if at times it is two steps forward and one step back.  The release
team's page at http://release.debian.org/sarge.html will continue to
document new release issues as we become aware of them, and we'll
continue striving to get update emails out to you on a semi-regular
basis.

Cheers,
-- 
Steve Langasek                                     [vorlon@debian.org]
Debian Release Team

[1] http://bjorn.haxx.se/debian/stalls.html
[2] http://lists.debian.org/debian-boot/2004/09/msg01085.html
[3] http://bugs.debian.org/release-critical/

Subject: release update: open blockers for the freeze

Hi,

during the last weeks, we achived some more progress towards the release.
Please keep up that good work!

There are a few items that should be resolved prior to the hard freeze, for
the very good reason that we don't want to spend time reviewing fixes if we
can use the same time (more productive) for fixing bugs.

So, what is open / fix needs to be confirmed:

Number of RC bugs
~~~~~~~~~~~~~~~~~
We have as of now about 160 release critical bugs in Etch in total,
about 110 which are open in both Etch and Sid (which is more or less the
only number we really could track during release of Sarge, to get some
comparison), and about 40 "old" RC bugs (i.e. older than 20 days, open in
both suites). Nothing too dramatic, but numbers need to go down more
before we should freeze.

Please, if you are maintainer of an RC-buggy package: Please upload your
fix *now*. If you are not yet a Debian Developer, there are plenty
people happy to sponsor your RC-bugfix upload - just send this
information to your RC bug report to make it happen.


Kernel 2.6.18 in Etch
~~~~~~~~~~~~~~~~~~~~~
There has been some progress on moving linux-2.6.18 to Etch, but
kernel-modules-extra needs another upload which fixes the
fails-to-build-from-source on some architectures. Also, even if
the kernel is in Etch, it needs to be stabilized for the release
(but that can happen post-freeze).


Release Notes
~~~~~~~~~~~~~
Nothing too dramatic, but we need to decide prior to full freeze about
the update order (as we expect lots of people to upgrade to testing
after the full freeze). "In good progress" summarizes the state fair.


The soft freeze continues
~~~~~~~~~~~~~~~~~~~~~~~~~
We have not yet reached a point where we believe there should be a hard
freeze of updates into etch, but we ask your continued support by avoiding
uploads that are likely to introduce regressions or be disruptive to other
packages so close before release.  Please do not upload new upstream
releases or start library transitions without discussing with the release
team, and please don't upload changes to unstable that are not meant for
Etch.  You can always upload such changes to experimental.

The release team depends on your cooperation with these guidelines in order
to keep the Etch release moving forward at the present pace.


Now, let's fix the remaining RC bugs, and then release Etch soon. Thanks for
your attention so far, and now, just grab any RC bug and fix it.



Cheers,
Andi
-- 
Debian Release Team
http://release.debian.org/

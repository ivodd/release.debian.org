Subject: bits from the release team

Hi,

another month has passed and DebConf happened, so we have a few more
changes to announce.


Release Goals
=============

After some discussion, we've decided to clarify the handling of "pet
release goals".  The set of "release blockers" established last year
remains the same; these issues are bugs of RC severity and covered under
the 0-day RC NMU policy. In addition we are extending the 0-day NMU
policy to cover certain endorsed "release goals" even when the bugs are
not release-critical but of severity important.

We believe this is warranted for these release goals because they
significantly improve the overall quality of the distribution across
packages, as well as etch's usefulness to a range of users.


Approved release goals as of now are:
- GCC 4.1 transition
- LSB 3.1 compatibility
- SELinux support
- pervasive ipv6 support
- pervasive LFS (large files) support
- new python framework (see below)

Please note that an issue appearing on this list does not mean the
release team takes responsibility for meeting these goals (though of
course we would like to see them met :).  It is the task of the
individual interested parties to make that happen.

For instance, the switch to GCC 4.1 can happen as soon as we have 20 or
less packages become RC-buggy by that switch, and only if it happens
rather soon.  There is progress, but there is still some way to go.



Python
======

There was a python BoF at DebConf where we discussed some changes to
python policy.  For the normal audience, the main result is that we
will get rid of pythonX.Y-foo-packages in the general case.  Also, python
transitions will get less painful in future.  For details, please refer to
the mails to the debian-python list.  We will also do the switch to python
2.4 in time for etch; due to the changes, it will be already less painful
this time.


X.org transition
================

If you track the progress of testing (e.g., [1]), you'll have noticed
that the Xorg 7.0 packages, which introduce modular sources and full FHS
support, are holding things up a bit.  We've been working to push these
fixes in, and you can expect this to unblock Real Soon Now <tm>.


Amd64 status
============

Amd64 is still in the progress of being added to testing.  It is now possible
to debootstrap etch on amd64, but some important packages are still missing.
Things will improve a bit as soon as X.org hits testing, but there is still
some work to be done.


Timeline
========

Now, let's please take a more detailed look at the time line:


         Thu 15 Jun 06:
         
    last chance to switch to gcc 4.1, python 2.4
    review architectures one more time
    last chance to add new architectures

    RC bug count less than 300


N-117  = Mon 30 Jul 06:

    freeze essential toolchain, kernels

    RC bug count less than 200

    
N-110  = Mon  7 Aug 06:

    freeze base, non-essential toolchain (including e.g. cdbs)

    RC bug count less than 180

    
N-105  = Mon 14 Aug 06:

    d-i RC [directly after base freeze]

    RC bug count less than 170

    
N-45   = Wed 18 Oct 06:

    general freeze [about 2 months after base freeze, d-i RC]

    RC bug count less than 80

    
N      = Mon  4 Dec 06:
    release [1.5 months for the general freeze]

    no RC bugs left!


It is still possible for us to reach this goal, but we need to switch gears
in order to make it happen.  Many uncoordinated uploads are still being
made to unstable which break other packages, and the RC bug count is going
up instead of down; it's time for us all to focus on making sure our
uploads help the RC bug count go in the other direction. So, please stop
making disruptive uploads, and work on getting things smoother now.

The RC bug tracker shows as of today about 350 release-critical bugs -
that is way too much. So, we ask you all to work on reducing the bug
number again.  At this point, we want to remember you that we have a
permanent BSP: You can upload 0-days NMUs for RC-bugs open for more than
one week.  However, you are still required to notify the maintainer via BTS
before uploading.  And of course, you need to take care of anything you
broke by your NMU.

Please see the Developers Reference for more details [3].


Looking at our release blockers published back in October[2], we had:
 - gcc 3.3 -> 4.0 toolchain transition
 - xfree86 -> xorg transition
these two are done
 - amd64 as an official arch (and the mirror split as a pre-condition
   for that)
almost done.
 - sorting out docs-in-main vs. the DFSG
has happened partially - the GR changed the GFDL-situation a bit. Some more
work here would be welcome.
 - sorting out non-free firmware
this is the next large blocker that needs to be addressed.
 - secure apt
secure apt is now part of testing.  However, we need to do something for key
management etc - so some small issues need to be resolved.


Release Bets
============

To provide some more motivation, Lars 'liw' Wirzenius said he will get tattooed
with the Debian swirl and all releases he worked on so far, if we manage to
release on or before December 4th. 



So much for now.  Thanks for your support.


Cheers,
-- 
Andi
Debian Release Team

[1] http://bjorn.haxx.se/debian/stalls.html
[2] http://lists.debian.org/debian-devel-announce/2005/10/msg00004.html
[3] http://www.debian.org/doc/developers-reference/ch-pkgs.en.html#s-nmu

#! TITLE: Bullseye moreinfo questions and reject reasons
#! SUBTITLE: How do I improve my unblock request?

Intro
=====

To avoid that we need to repeat ourselves, we are collecting our standard
requests for info and our reasons for rejects on this page.

Be reminded that the release team consists of humans. We don't like turning
down a request for an unblock. So please consider carefully if the unblock is
really warranted and don't just "try".

Moreinfo
========

New upstream release
--------------------

Upstream releases are in general not acceptable during the freeze. However,
every rule has exceptions. Please don't take this path lightly.

During the freeze we only want targeted fixes for bugs of severity important
and higher (according to the BTS definition). It is possible that a new
upstream release *is* a targeted bug fix release. However, the maintainer has
to do the checking. They should convince themselves 100%, so they can convince
us. Questions to ask and answer are:

1. Is this a targeted bug fix release, and how does that show?

1. What are the risks of the changes for the quality of the Debian release?

1. Is there a policy that describes what upstream considers acceptable for this
upstream release?

1. Does that policy align with our bug severity important or higher?

1. Does upstream test thoroughly?

1. Has this package seen new upstream version uploads to stable in the past to
facilitate security support?

1. Etc...

### No Debian bugs

We care for bugs that are reported in the Debian BTS, but that doesn't mean
that it's all we care about. If the maintainer evaluates the changes done by
upstream and convinced themselves that they all qualified, they may be able to
convince us, but it needs explaining.

Changes without explanation
---------------------------

During the freeze, please mention *all* changes to the package in the
debian/changelog. If you don't, then please be verbose in your request for an
unblock. The release team members can't read minds and normally a lot of
requests need to be processed. The more relevant information is put directly
into the initial request, without the need to follow all kind of links to all
kind of places, the easier it is to process as much requests as possible.

Unblock requests without a response in a long time
--------------------------------------------------

It happens that your unblock request is met with silence. Typically this means
that the review is difficult, e.g. because of a large amount of changes or
because the unblock should actually be rejected. Consider checking if your
unblock request is following our guidelines. If there is infomation you can
provide that makes the review easier, e.g. by explaining rational of certain
changes, choping changes into logical pieces, or pointing at upstream release
policy, don't hesitate to add that in a follow-up to the unblock bug. Maybe the
unblock should be rejected, but nobody wanted to do that yet (rejecting a
request isn't nice), consider closing the unblock request yourself.

Reject
======

Unreviewable new upstream version or with not important changes
---------------------------------------------------------------

Upstream releases are not acceptable during the freeze, but see about the
possible exceptions in the moreinfo section. If it's clear that the new
upstream release doesn't qualify for such an exception, an unblock request will
be rejected.

If the new upstream release includes fixes for bugs of severity important and
higher (according to the BTS definition), which you want bring to testing,
you'll have to backport them to the version of your package currently in
testing. You'll have to "undo" your new upstream release upload to unstable by
mangling the version number, e.g. by using the +really style:
`new-upstream-version`+really`upstream-version-in-testing`-1, upload the
package to unstable and request an unblock for that.

Debhelper compat bump
---------------------

The debhelper compat mechanism is there to stabilize debhelpers
behaviour. Bumping it is not allowed during the freeze, as it requires very
careful checking for unintended behavior.

#! TITLE: Bullseye Freeze Timeline and Policy
#! SUBTITLE: let's release!

DRAFT
=====

The freeze for bullseye will happen according to the following timeline:

 * TBC - Transition freeze
 * TBC - Soft-freeze
 * TBC - Full-freeze

These phases are fully explained below, but condense down to the following summary:

    +---------------------+-------------------------------+---------------------+----------------------+
    | Effect \ Milestone  |       Transition freeze       |     Soft-freeze     |      Full-freeze     |
    +---------------------+-------------------------------+---------------------+----------------------+
    | Major transitions   |  No new transitions           |          No         |          No          |
    | Max. change level   |  No large/disruptive changes  | small, targeted fix |       RC (all)       |
    |                     |                               |                     | important (optional) |
    | Migration age req.  |  Standard (2/5/10d)           |     10 day delay    |  Standard (2/5/10d)  |
    | Autopkgtest bounty  |  Yes                          |          No         |         Yes          |
    | New src packages    |  Standard automatic migration |          No         |          No          |
    | Re-entry to testing |  Standard automatic migration |          No         |          No          |
    | Migration rules     |  Standard automatic migration | Some manual reviews |  100% manual review  |
    +---------------------+-------------------------------+---------------------+----------------------+


Before the freeze
=================


Plan your changes for bullseye
----------------------------

If you are planning big or disruptive change, check the timeline to see if
it's still realistic to finish them before the transition freeze. Keep in mind
that you might need cooperation from other volunteers, who might not
necessarily have the time or capacity to work on this. You can stage your
changes in experimental to make sure all affected packages are ready before
uploading the changes to unstable.


Fix release-critical bugs
-------------------------

You can help the development of bullseye by fixing RC bugs, before and during
the freeze. An overview of bugs can be found on
<a href="https://udd.debian.org/bugs/">the UDD bugs page</a>.

You can also join the #debian-bugs irc channel on irc.oftc or join a
<a href="https://wiki.debian.org/BSP">Bug Squashing Party</a>.


Testing migration
-----------------

The 'standard' testing migration rules apply:

 * automatic migration after 2/5/10 days for priority high/medium/low packages
 * faster migration for packages with successful autopkgtests
 * no migration for packages that trigger autopkgtest regressions
 * no migration for packages with new RC bugs or piuparts regressions
 * migration for packages only allowed if all their binary packages have been built on buildds
 * auto-removal of non-key packages
 * packages not in testing can migrate to testing
 * no manual review by the release team

Transition Freeze
=================

Starting TBC, **new transitions and large/disruptive changes** are no
longer acceptable for bullseye.

Testing migration
-----------------

Changes in the rules for testing migration:

 * None.

The 'standard' testing migration rules apply.

Soft Freeze
===========

Starting TBC, only **small, targeted fixes** are appropriate for
bullseye. We want maintainers to focus on small, targeted fixes. This is
mainly, at the maintainers discretion, there will be no hard rule that will be
enforced.

Please note that new transitions or large/disruptive changes are not
appropriate.

The release team might block the migration to testing of certain changes if
they might cause disruption for the release process.

Increased delay for all testing migrations
------------------------------------------

The testing migration delay of all packages will be increased to 10 days. This
means the rate of changes to testing will slow down.

The minimum delay of 10 days will also apply to packages with successful
autopkgtests.


No new packages and no re-entry to testing
------------------------------------------

Packages that are not in testing will not be allowed to migrate to testing.
This applies to new packages as well as to packages that were removed from
testing (either manually or by auto-removals). Packages that are not in bullseye
at the start of the soft freeze will not be in the release.

Please note that packages that are in bullseye at the start of the soft freeze
can still be removed if they are buggy. This can happen manually or by the
auto-removals. Once packages are removed, they will not be allowed to come
back.


Testing migration
-----------------

Changes in the rules for testing migration:

 * manual review by the release team for some packages
 * migration delay always at least 10 days
 * no faster migration for packages with successful autopkgtests
 * packages not in testing can not migrate to testing

The following rules still apply:

 * no migration for packages that trigger autopkgtest regressions
 * no migration for packages with new RC bugs or piuparts regressions
 * auto-removal of non-key packages

Full freeze
===========

Starting TBC, packages can only migrate to testing after manual review
by the release team.

We have some criteria for what changes we are going to accept.  These
are listed below.  These criteria
will become more rigid as the freeze progresses.

The release managers may make exceptions to these guidelines as they
see fit. **Such exceptions are not precedents and you should not
assume that your package has a similar exception.** Please talk to us
if you need guidance.

Please talk to us early and do not leave issues to the last minute. We
are happy to advise in case you need the release team's help to fix RC
bugs (e.g.  to remove an old package)


Testing migration
-----------------

Changes in the rules for testing migration:

 * manual review by the release team for all packages, based on the criteria
   listed below

The following rules continue to apply:

 * no migration for packages that trigger autopkgtest regressions
 * no migration for packages with new RC bugs or piuparts regressions
 * auto-removal of non-key packages
 * packages not in testing can not migrate to testing

Changes which can be considered
-------------------------------

 1. targeted fixes for release critical bugs (i.e., bugs of severity critical, grave, and serious) in all packages;
 1. fixes for severity: important bugs in packages of priority: optional, only when this can be done via unstable;
 1. translation updates and documentation fixes that are included with fixes for the above criteria;

Note that when considering a request for an unblock, the changes
between the (proposed) new version of the package in `unstable` and
the version currently in `testing` are taken in to account. If there
is already a delta between the package in `unstable` and `testing`,
the relevant changes are all of those between `testing` and the new
package, not just the incremental changes from the previous `unstable`
upload. This is also the case for changes that were already in
`unstable` at the time of the freeze, but didn't migrate at that
point.

We strongly prefer changes that can be done via unstable instead of
testing-proposed-updates. If there are unrelated changes in unstable, we ask
you to revert these changes instead of making an upload to
testing-proposed-updates. Hence, and also because it may impact other packages
in unstable that try to migrate to bullseye, it is recommended that, during the
freeze, you do not upload to unstable any changes for which you do not intend
to request an unblock.


Applying for an unblock
-----------------------
 1. Prepare a **source** `debdiff` between the version in `testing` and `unstable` and check it carefully
 1. Use `reportbug` to report an unblock bug against the `release.debian.org` meta-package. Attach the source diff. Include a detailed justification of the changes and references to bug numbers.
 1. If the diff is small and you believe it will be approved, you can upload it to unstable before filing the unblock request to avoid a round-trip.
 1. Depending on the queue, there may be some delay before you receive further instructions.

If you are unable to bring your fix through unstable, for example because your
package in unstable is dependent on a version of another package that isn't
available in bullseye and that is not being unblocked, the release team can
grant you permission to use the `testing-proposed-updates` mechanism. Make sure
you fix the issue in unstable following the rules outlined above and prepare a
no-change upload targeting `testing-proposed-updates` but **do not upload it**,
and then contact us through an unblock bug. testing-proposed-updates is not
meant to prevent "ugly" version numbers for packages that already have a newer
upstream version in unstable. You're requested to revert the upstream version
instead, e.g. by using a `new-version`+really`old-version` versioning scheme.

Targeted fixes
--------------

A targeted fix is one with only the minimum necessary changes to
resolve a bug. The freeze process is designed to make as few changes
as possible to the forthcoming release. Uploading unrelated changes is
likely to result in a request for you to revert them if you want an
unblock.

Some examples of changes that are undesirable during a freeze:

 1. dropping a -dbg package in favour of -dbgsym
 1. adding a new systemd unit in place of an init script
 1. bumping the debhelper compat level

Removing packages from testing during the freeze
================================================

Throughout the freeze, we will continue to remove non-key
packages with RC bugs in testing and their reverse dependencies automatically.
As usual, the auto-removal system will send a notice before this happens.
Please note that it is not enough for the bug to be fixed in unstable.
The fix (in unstable or testing-proposed-updates) must be done in such a way that
the fixed package can be unblocked and allowed to migrate to testing, based on the rules
listed above. **You must contact us to ensure the fix is unblocked - do not rely on
the release team to notice on their own.**

Manual removal may still happen without warning before the standard auto-removal
periods have passed, when the package is blocking other fixes.

After the soft freeze begins, removed packages will **not** be permitted to re-enter testing.
